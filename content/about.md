---
title: "About"
date: "2020-02-10"
aliases: ["about-us", "about-wolfdefi", "contact"]
---

My name is Zak, and this is my space for exploring decentralized finance, tokens, governance, gaming, security, and some other things. 

If you'd like to keep up or want to chat, you can find me on [Telegram](https://t.me/inmathswetrust), connect on Twitter [@wolfdefi](https://twitter.com/WolfDefi), or reach out directly via [email](info@wolfdefi.com). Stay up, friend.

{{< image src="/img/wolfdefi.png" alt="Hello Friend" position="center" height="150" width="150" style="border-radius: 8px;" >}}
