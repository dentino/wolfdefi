---
title: "I DEGEN Podcast"
date: 2022-10-07T14:43:12-06:00
draft: false
images: ["/img/i-degen-banner.png"]
tags:
  - I,DEGEN
  - Crypto
  - podcast
  - Ethereum
  - Bitcoin
---

![](/img/i-degen-banner.png)

Recently my friend [Hunt](https://twitter.com/Hunthk11) and I started a new podcast. It's a weekly show covering topics related to crypto hacks and security. For each episode, we run through a weekly review of crypto hacks and pick a topic or two to dig into and explore in more depth. We've also been testing out [Open Source Audio Audits](https://idegen.fm/episodes/e13-niftyapes-open-source-audit-w-kevin-seagraves-zach-herring-9-1-2022) where we chat with founders and engineers about how they've approached securing their DeFi or crypto projects.  

## Why I, Degen? 
Before crypto degens emerged, the term degen was mainly associated with gamblers. In the crypto context, it's often associated with NFT or coin traders that take crazy risks in hopes of earning massive profits. And gamblers too of course.  

However, our show isn't about gambling, apeing into NFTs, or leveraged-up trading. So why call it 'I, Degen'? While I found the name pretty clever, the main point behind using I, Degen was that crypto has never really had a good reputation. **Outside the crypto bubble, you're considered a degen if you're involved with crypto whether you like it or not, even if you're signaling high on the altruistic side.**  At the same time, the idea that taking risks makes you a degen is a bit suspect. 

> if we consider games of chance immoral then every pursuit of human industry is immoral, for there is not a single one that is not subject to chance, not one wherein you do not risk a loss on the chance of some gain. - [Thomas Jefferson, 1826](https://founders.archives.gov/documents/Jefferson/98-01-02-5845)

As the show evolves, we've begun incorporating viewpoints from outside the crypto bubble. While tribes form, rise and fall inside crypto, it's essential to zoom out and take in the big picture. That picture includes a growing faction of 'nocoiners' who see crypto as nothing more than a series of Ponzi schemes. I think this gives our show a more realistic and grounded feel.  

## Beyond the Name
Inspired by Lex Friedman, Sam Harris, and others that explore topics in a long-form manner, I wanted to create a show focused on accuracy, not speed and sensationalism. Similar to [Daniel Miessler's Unsupervised Learning](https://danielmiessler.com/newsletter/), but concentrated on crypto security specifically. 

Please listen and let us know what you think! 

You can find the show's main webpage at [idegen.fm](https://idegen.fm). You can also find us on [Spotify](https://open.spotify.com/show/6iLpjIffcI6OrbKORwM0bP), [Apple](https://open.spotify.com/show/6iLpjIffcI6OrbKORwM0bP), and [Google Podcasts](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL2ktZGVnZW4=). Follow us on Twitter [@Idegenfm](https://twitter.com/idegenfm). 

Also, I'm planning to post extensive weekly show notes [here](/i-degen). Have a look! 

Happy listening, 

-Zak




 