---
title: "Forging the Quadratic Lands - GTC Token, GitcoinDAO, & Token Distributor "
date: 2021-08-01T16:51:00-06:00
draft: false
toc: false
images: ["/img/quadratic_lands_2.svg"]
tags:
  - Gitcoin
  - GTC
  - GitcoinDAO
  - QuadraticLands
  - solidity
---

{{< image src="/img/quadratic_lands_2.svg" alt="quadratic lands" position="center"  style="border-radius: 4px;" >}}

### Adventures in Sybil Attacks & Yield Farming Games
In the late summer of 2020, a friend pointed me to a Tweet that Kevin Owocki posted looking for collaborators on sybil attack resistance strategies. "You should talk to Kevin about sybil attacks," my friend said. 

It took a couple of weeks to get a call scheduled with Kevin, and we did talk about sybil attacks. Gitcoin's novel quadratic funding mechanism allocates matching pool funds to a given grant based on the number of individual contributors, not the grant contribution amount. 

As a simple example, a hundred one dollar donations would reward a grant more matching fool funds than a single one hundred dollar donation. Thus, if an individual (or group) could successfully create fake identities and many small contributions (ie, a sybil attack), they could game the Gitcoin Grants system out of matching donor funds. 

As the Gitcoin Grants ecosystem grew, so did the donor matching pool funds and the payout for a successful sybil attack. I thought about the problem a lot, and I hit a lot of dead ends.  

A few weeks later, Kevin messaged me. I was not looking forward to sharing my conclusion that sybil resistence might be an unsolvable problem in this context. Fortunately, the conversation went different direction altogether. 

Kevin wasn't interested in talking about sybil resistance at all, "What do you want to do?" he asked. "Like, if you could build anything you want, what would it be?". I told him that I wanted to build games again.

A few weeks later, I talked to Kevin again. "I want to build a yield farming game," he said. Yams had just dropped, and we were experiencing the birth of the yield farming meme in real-time. Although I was a bit confused, this was enough for me to quit my job and go headfirst into the cryptoverse again. 

### Enter the Quadratic Lands
Once we worked everything out and it was time to get started, the scope evolved. Gitcoin was ready to take its first big step towards decentralization by launching a governance token and doing a retroactive drop. I was lucky enough to take on the engineering lead role for creating the token (what would eventually be named GTC), the governance contracts, the token distribution contracts, and the web portal tieing it all together, The Quadratic Lands. 

This nine-month journey was fantastic and still stands as a high point for me in my career. Working on this project at Gticoin was strange and beautiful chaos. I met and collaborated with many genuinely brilliant people.  

If you want to learn more about the technical challenges and overall engineering process, please check this article I wrote for the Gitcoin Blog: [Architecting & BUIDLing the GTC Token Distributor](https://go.gitcoin.co/blog/architecting-buidling-the-gtc-token-distributor).  

You can find the [QuadraticLands website here](http://decentralized.quadraticlands.com/). 

The smart contracts for the project are on [github here](https://github.com/gitcoinco/governance). 

Sadly, because we created and built the QuadraticLands app on a private fork of the main Gitcoin website, most of work on the web app itself was pushed as a single commit to the public master branch. In case you're interested in checking that out, [you can find that commit here](https://github.com/gitcoinco/web/commit/61eb3f998f41f11979f94cfedeadf221a0c6c16d). 












