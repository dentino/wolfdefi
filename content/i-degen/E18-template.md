---
title: "E18 - [title]"
date: 2022-10-11T14:12:10-06:00
draft: true
images: ["/img/i-degen-banner.png"]
tags:
  - I,DEGEN
  - Crypto
  - podcast
  - Ethereum
  - hacking
  - defi
---

![](/img/i-degen-banner.png)

|| Listen at [idegen.fm](https://idegen.fm) 🟢 Contact [@idegenfm](https://twitter.com/idegenfm) 🟢 Show notes [wolfdefi.com](https://wolfdefi.com/i-degen) ||

---

#### Intro
Welcome to I, Degen - A podcast about crypto technology, security, and culture. With a healthy balance of enthusiasm and skepticism, we dig into a weekly look at crypto, cutting through the misinformation and hype in search of signal in the noise.

#### Episode Summary

# I,Degen - Weekly

1. Binance Bridge Hack
2. Sovryn Hack 
    - https://cryptopotato.com/bitcoin-defi-protocol-sovryn-gets-hacked-for-over-1-million/
    - https://www.sovryn.app/blog/interim-exploit-update
    - https://rekt.news/sovryn-rekt/
3. Transit Swap Hack from Oct 2nd, hacker returns some of the funds [](https://www.coindesk.com/business/2022/10/03/transit-swap-exploiter-returns-large-chunk-of-289m-hack/) and [SlowMist](https://slowmist.medium.com/cross-chain-dex-aggregator-transit-swap-hacked-analysis-74ba39c22020)

# I, Degen - Deep Dive 

# I, Degen - Freestyle Convo

# I, Degen - Personal Hack Attempt of the Week

[[[Outro]]]

:::warning
We do our best to report accurately on the topics we  discuss but we're not always going to get everything right. Please comment here or reach out to us @idegenfm with corrections or comments!
:::
