---
title: "E19 - Alpha Hunting w/Threat Intel Bots & Devcon 6 Security Talks Gem Hunt PT1"
date: 2022-10-21T06:50:52-06:00
draft: false
images: ["/img/i-degen-banner.png","/img/i-degen/i-degen-e19-devcon-6.png"]
tags:
  - I,DEGEN
  - Crypto
  - podcast
  - Ethereum
  - hacking
  - defi
---

![i-degen-banner-image](/img/i-degen-banner.png)

---
╠ Listen [idegen.fm](https://idegen.fm) ✪ Contact [@idegenfm](https://twitter.com/idegenfm) ✪ Show notes [wolfdefi.com](https://wolfdefi.com/i-degen) ╣

---

#### Intro
Welcome to I, Degen - A podcast about crypto technology, security, and culture. With a healthy balance of enthusiasm and skepticism, we dig into a weekly look at crypto, cutting through the misinformation and hype in search of signal in the noise.

**Listen to I, Degen E#19 [directly on idegen.fm](https://idegen.fm/episodes/e19-alpha-hunting-w-threat-intel-bots-devcon-6-security-talk-gem-hunt-1), on [Spotify](https://open.spotify.com/episode/6LMCgYruhZdJwAnvBzG3gD?si=8fbedeec746847ba), [Apple Podcasts](https://podcasts.apple.com/us/podcast/i-degen/id1643133657?i=1000583487229), or [various other places](https://idegen.fm/subscribe).**

#### E19 Summary 
Finally, a break in the storm as things calmed down in the third week of a brutal October. This week, we will discuss the Moola Markets price manipulation and the Cosmos vulnerability. We'll dive deep into a few choice DEVCON 6 security talks and follow the rabbit hole from one of the talks into decentralized threat intel bots.  

![e19-devcon6-wizard](../../img/idegen/i-degen-e19-devcon-6.png)

### Upcoming Events of Interest
1. [Forta Bot Competition](https://docs.forta.network/en/latest/contest9-forta/) - Attacked Protocol/Asset Source Identification Bot. October 11th-November 1st. They will only take the first ten submissions. 

2. ETH Lisbon Hackathon, 28-30 Oct

3. Rekt is doing a 'secret' meetup in Lisbon as well

## I, Degen - Weekly Crypto, Security, & Other News Headlines of Interest

1. [4:04pm UTC on 10/18/22 -Moola Markets price manipulation attack](https://twitter.com/Moola_Market/status/1582588102790434816) 

> Using initial funding of 243k CELO (~$180k), the attacker supplied 60k CELO in order to borrow 1.8M of the protocol’s native token, MOO, which could itself be used as collateral to borrow against other assets.

> Then, using the remaining CELO to buy MOO on Ubeswap, the attacker pumped the price of their MOO collateral allowing them to borrow the remaining assets on the protocol, draining all liquidity

- [Moola Markets Tweet](https://rekt.news/moola-markets-rekt/)
- [The Attacker returns 9MM in exchange for reward](https://forkast.news/headlines/moola-market-defi-hacker-returns-us9-mln-stolen-funds-for-bounty/)
    
2. October 15th, One of the Mango Markets manipulators came [out](https://twitter.com/avi_eisen/status/1581326197241180160?t=jUexJsmReMjYqiAzzUbhEQ&s=35) and became a meme for 'deploying a highly profitable trading strategy'. Later (October 18th), he [proposed a ](https://twitter.com/avi_eisen/status/1582368460645945345?s=20&t=Zl0KLPbIQsu5EMgh9QCucA) solution to make lending protocols more robust (less susceptible to price manipulation attacks). The Tweet ignites a question again of if this was a hack or not.

3. [October 13th, IBC Security Advisory Dragonberry, critical security vulnerability impacts all IBC-enabled Cosmos chains, for all versions of IBC](https://forum.cosmos.network/t/ibc-security-advisory-dragonberry/7702)

> A chain is safe from the critical vulnerability as soon as ⅓ of its voting power has applied the patch. Chains should still seek to patch to ⅔ as quickly as possible once the official patch is released.

No reports of exploitation in the wild. 

- [Cointelegraph report](https://cointelegraph.com/news/cosmos-co-founder-says-a-major-security-vulnerability-has-been-uncovered-on-ibc)

The article also notes that fewer hacks have taken place on Cosmos. I wonder why that is? 

Also, [issue this different issue for Cosmos](https://twitter.com/Verichains/status/1580168202767990785):

![verichans-cosmos-bridge-tweet](../../img/idegen/verichains-cosmos-bridge-tweet.png)

[And Cosmos-SDK Security Advisory Dragonfruit from October 8th](https://forum.cosmos.network/t/cosmos-sdk-security-advisory-dragonfruit/7614)

> Recently, the core Cosmos team became aware of a high-severity security vulnerability that impacts all users of the IAVL merkle proof verification (“RangeProof”) system, for all versions of the IAVL software. 

Nice work to all involved to uncover and path these before exploit. 

4. [October 2nd & 10th, Ray Network - Franken Attack. ](https://raynetwork.medium.com/franken-attack-80140b63749f)

RayNetwork, a DeFi ecosystem for Cardano was attacked. 

> On October 3 and October 10, 2022 an attacker attacked RayStake by spoofing a stake key in the payment address, causing the vending machine to send rewards of a specific stake key to the attacker’s address.


> the attacker accessed a total of 5,554,113 XRAYs and 112 XDIAMONDs. He sold these funds through Minswap DEX and Sundaeswap for 412,253 ADA.

worth ~139,540.95 USD at time of this writing. 

5. [OlympusDAO Bond Pool Drained of ~300k with improper validation bug](https://twitter.com/peckshield/status/1583416829237526528) 

> - DeFi protocol Olympus DAO lost almost $300,000 in OHM tokens in a security exploit today. 
> - The attacker later returned all of the funds following a negotiated deal. 

This was a new Bond Protocol contract that was still in testing. So, there were limited funds in the pool. 

> The hacker has returned all of the stolen tokens. The team successfully tracked the perpetrator, after which the hacker agreed to return the funds, a spokesperson from Olympus DAO told The Block.

- [TheBlock](https://www.theblock.co/post/178927/hacker-drains-olympus-daos-smart-contract-of-300000)

6. [Two men sentected to ~2 years for crypto SIM swapping scheme](https://www.justice.gov/opa/pr/two-men-sentenced-nationwide-scheme-steal-social-media-accounts-and-cryptocurrency)

> According to court documents, Meiggs and Harrington targeted executives of cryptocurrency companies and others who likely had significant amounts of cryptocurrency and those who had high value or “OG” (slang for Original Gangster) social media account names. 

## E19 Devcon 6 - Security Talk Gem Hunt

### 1. [Rug Life: Using Blockchain Analytics to Detect Illicit Activity and Protect Yourself](https://archive.devcon.org/archive/watch/6/rug-life-using-blockchain-analytics-to-detect-illicit-activity-track-stolen-funds-and-stay-safe/?tab=YouTube)

#### Presenter: Heidi Wilder - Special Investigations at Coinbase 

#### Talk Abstract
> Learn how to use blockchain analytics to identify and protect yourself from the latest rugs, hacks, and scams.
The purpose of this talk is to discuss: - How to (automatically) identify illicit activity on the blockchain - Typologies of the latest rugs, hacks, and scams - Tracing where funds from a latest rug/hack/scam have gone - How to protect yourself as a dev

#### Talk Notes & Highlights 
Detection Methods - 
    - alerts to monitor for large flows of funds 

Detecting weird stuff in advance
- devs: Monitor what is going on with your contracts. 
> Nomad could have see wierd stuff going back to July and probably prevented 

- use caution with twitter investigators. During Mango Markets price manipulation, she saw various addresses being thrown out that weren't involved in the hack 

- cool look at the tracing of the Ronin funds.

![Basic OPSEC](../../img/idegen/rug-life-devcon-6-basic-security.png)

- Social Engineering attacks are a common thread as the initial attack vector. Particular nation-states are known to use it often. 

- Privacy Protocols are relatively easy to track enormous volume hackers because they move so much at once

### 2. [Crosschain Security Considerations for the Degen in All of Us](https://archive.devcon.org/archive/watch/6/crosschain-security-considerations-for-the-degen-in-all-of-us/?playlist=Security&tab=YouTube)

#### Presenter: [Layne Haber](https://twitter.com/laynehaber)
#### Talk Abstract:
> Crosschain applications (xApps) are often considered too risky, but this viewpoint is divorced from reality. People **will** use these applications and it is our responsibility to understand the security implications. xApp developers must be able to reason about concurrency and asynchrony across two different networks, as well as understand the trust assumptions introduced by the data transport layer. By understanding this, we can allow users to engage in risky behavior in the safest way.

- Contagion Risk 

[What is contagion?](https://www.nasdaq.com/articles/the-crypto-market-is-not-immune-to-contagion-risk) 
> Generally, in economics, a contagion is the spread of an economic crisis from one financial institution, market, or region to another.

    Taxonomy of bridges 
    - native verified - complicated 
    - externally verified 
    - optimistically verified 

Bridges are hard. 

Validation of consensus and state transition is coming soon and will likely be the gold standard but not ready for prime time yet.  

Type of security 
![](../../img/idegen/layne-hayber-devcon-6-bridge-security.png)

- Economic - How much would it cost to corrupt your system?

- Implementation - How complex is the implementation of your system? Also, your dev environment. Audits, fuzzing, bug bounties, etc. 

- Environment - How can your system handle underlying domains with low economic security?

    Winner: Environment Security
        1. Optimistic 
        2. External 
        3. Native

Most bridge hacks we see now are Implementation based. 

    Q: Why do we see so many hacks? 
    A: New teams, resource-constrained, and shortcuts are taken. 

What can users do?
![Practical Considerations for bridge users](../../img/idegen/layne-haber-devcon-6-2.png)

[Lindy Effect](https://en.wikipedia.org/wiki/Lindy_effect) resets on upgrade. 

> Lindy Effect is a theorized phenomenon by which the future life expectancy of some non-perishable things, like a technology or an idea, is proportional to their current age. 

### 3. [Decentralized Threat Detection Bots](https://www.youtube.com/watch?v=5EV3VlKeBI4)

Presenter: [Jonathan Alexander](https://twitter.com/jalex206) CTO @ Open Zepplein & Co-founder [@Fortanetwork](https://twitter.com/FortaNetwork)

[Devcon link](https://archive.devcon.org/archive/watch/6/decentralized-threat-detection-bots/)

#### Talk abstract: 
> Decentralized threat detection bots are a recent area of research and development for protecting the ecosystem. This talk will cover concepts and recent research on detection bots and implementation patterns including heuristic-based, time-series based, multi-block, and TX simulation. Examples involving prior exploits will be included, as well as tools, limitations, the potential for automated threat prevention, and areas for further research.

#### Talk notes & highlights:
> Multiple leading security audit firms are beginning to make recommendations on post-deployment smart contract monitring.     

    - Protocol assumption invariants 
    - state of critical protocol variables 
    - known protocol risks that have been considered acceptable 
    - privileged protocol functionality and transfer of privilege 
    - on-chain / off-chain /cross-chain synchronization (oracles, bridges)
    - external contracts that the protocol relies on or is exposed to 
    - identified user and protocol attack patterns 

Smart Contract Attack Stages 

    1. Funding - new account, mixer, CEX, bridge transfer
    2. Preparation - contract deployment 
    3. Exploitation - flash loans, transfers, flashbots, re-entrancy, etc - more than 1/2 attacks are non-atomic (more than one tx)
    4. Laundering - 

![attack contracts differ from benign contracts!](../../img/idegen/decentralized-threat-detection-bots.png)

Similar results with bytecode analysis 

Fraud-based attacks like Ice Phishing can predict with 95% precision with heuristics!   

 - [app.forta.network](https://app.forta.network) 
 - [https://forta.org/](https://forta.org)

> Forta is a real-time detection network for security & operational monitoring of blockchain activity

Lots of great bot examples, including ML models to do things like:
    - Smart Price Change Detector
    - Time Series Analyzer 
    - Contract Destructor 

Challenges:
    - Atomic Attacks 
    - private txs 
    - monitoring secrecy 
    - response latency 

Future Areas of research:
    - private scan pools 
    - mempool Tx Scanning 
    - on-chain alerts 

This opens the question up:
### Can we find Threat Intel Alpha? Let's see... 

[https://explorer.forta.network/](https://explorer.forta.network/)

### Other Cool Devcon 6 Talks we reviewed 

- [Battle of the Bridges: Untangling the Tradeoffs of Various Bridge Designs](https://archive.devcon.org/archive/watch/6/battle-of-the-bridges-untangling-the-tradeoffs-of-various-bridge-designs/?playlist=Security&tab=YouTube)

- [How to Not Be Worth Kidnapping](https://archive.devcon.org/archive/watch/6/how-to-not-be-worth-kidnapping/?playlist=Security&tab=YouTube) - Ryan Lackey

- [The $10B Problem - web3 Security Against Coordinated Adversaries](https://archive.devcon.org/archive/watch/6/the-dollar10b-problem-web3-security-against-coordinated-adversaries/)
Julia Hardy & Adam Hart from Chainalysis 

> Bored Ape Yacht Club Discord hacked, Ronin Bridge compromised, the news articles are fraught with Ethereum exploits. Chainalysis has identified that these attacks are often executed by a small circle of well-funded, well-coordinated adversaries. In this session, Chainalysis examines how these actors operate, how we investigate the flow of funds to try to disrupt attacks, and how the web3 community can work together to raise costs for attackers using the transparency of public blockchains.

Tether Approval Scam - 83MM in a year! more than 20k victims have been tracked in connection with scam. 

## I, Degen E19 - Links of Interest 

1. [Uniswap V3 Development Book](https://uniswapv3book.com/) - Great free book that will guide you though the development of a decentralized application. Book was funded though a Uniswap grant. 

2. I added this to last week's notes too, but I wanted to mention it on the show as well. Reminder to revoke old contracts that have approval on your addys at [revoke.cash](https://revoke.cash/)

3. [Mintlayer - A story of deceit, betrayal, and millions of dollars of investor’s money squandered in unbridled luxury](https://www.albertodeluigi.com/2022/10/12/crypto-project-investors-money-lost-in-luxury-goods/)

4. [Interesting allegation of crypto bots being caught gaming twitter polls on Reddit](https://www.reddit.com/r/CryptoCurrency/![]comments/y8fe5u/cardano_criticisms/)

![bad-cardano-bots](../../img/idegen/bad-cardano-bots.png)

It would be interesting to see if we can find proof of this. 

5. [Mudit Gupta, CISO at Polygon weighs in on Bogota safety concerns](https://twitter.com/mudit__gupta/status/1581735409708544000?s=12&t=2wX3UicKzB0-78uTrjMncw)

6. [Hackers continue to use TornadoCash desipte sanctions](https://beincrypto.com/hackers-continue-to-use-tornado-cash-despite-sanctions/)

7. [Hans Niemann files defamation suit against Carlson, Nakimura, & Chess.com](https://twitter.com/HansMokeNiemann/status/1583164606029365248?s=20&t=SO_dqThvKdh_X-mcFjrKfQ)

## I, Degen E19, Maybe list 

- [Relay Reward Strangess](https://twitter.com/0xdef1/status/1581390532416135168)

- Oddly, the term honeypot has a different meaning in crypto security lexicon. It seems to describe anything that is a target for attackers, not a target that has been expressly set up to draw in attackers.   

 - [Tech lead at Arbitrium drops trivial vuln in BitBTC bridge (to Optimism)](https://twitter.com/PlasmaPower0/status/1582176532985880576?t=abodESuGkr9Xcd8Erlhf_w&s=19)

![lee-bousfield-tweet](../../img/idegen/lee-bousfield.png)

- Questions about responsible disclosure
- If any funds were actually at risk
- Maybe clout farming?

### Outro
___
**We do our best to report accurately on the topics we discuss, but we're not always going to get everything right. Please contact us [@idegenfm](https://twitter.com/idegenfm), [@WolfDeFi](https://twitter.com/WolfDefi), [@Hunthk11](https://twitter.com/Hunthk11) with corrections or comments!**
