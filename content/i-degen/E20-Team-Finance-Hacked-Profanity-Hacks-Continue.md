---
title: "E20 - Team Finance, FriesDAO, & Layer2DAO Hacked - Profanity Woes Continue - 11/1/2022"
date: 2022-11-01T9:00:49-06:00
draft: false
images: ["/img/i-degen-banner.png"]
tags:
  - I,DEGEN
  - Crypto
  - podcast
  - Ethereum
  - hacking
  - defi
---

![i-degen-banner-image](/img/i-degen-banner.png)

---
╠ Listen [idegen.fm](https://idegen.fm) ✪ Contact [@idegenfm](https://twitter.com/idegenfm) ✪ Show notes [wolfdefi.com](https://wolfdefi.com/i-degen) ╣

---

#### Intro
Welcome to I, Degen - A podcast about crypto technology, security, and culture. We balance hype and hate, cutting through the misinformation and ego in search of the signal in the noise.

**Listen to I, Degen E#20 [directly on idegen.fm](https://share.transistor.fm/s/f45512aa), on [Spotify](https://open.spotify.com/episode/03HL22iAT62jMPlYZBgkIG?si=9a55b68ee7eb4f85), [Apple Podcasts](https://podcasts.apple.com/us/podcast/i-degen/id1643133657?i=1000584686333), or [various other places](https://idegen.fm/subscribe).**

#### E20 Summary 
October is finally over, and we're going a mile wide and an inch deep on this week's crypto security news. It would have been a relatively calm week if it weren't for the ongoing profanity attacks. Let's dig in.  

## I, Degen - Weekly Crypto Security Headlines of Interest

### DeFi Hacks of the Week

1. [October 22rd, Investment DAO Layer2DAO announced on Twitter that a hacker was able to gain access to a platform multisig on Optimism and drain 49,950,000 L2DAO](https://news.coincu.com/136224-layer2dao-hacked-for-50-million-l2dao/) 

    - Hack loss was roughly $384,615 USD at the time of the attack, with L2DAO token at ~0.0077. Price has almost fully recovered as of this writing. 

    > Today a hacker was able to gain access to an Layer2DAO multisig on Optimism and drain 49,950,000 L2DAO tokens. They dumped some of the stolen tokens but we were able to negotiate and repurchase the remaining 31,239,677 tokens. - [@Layer2DAO](https://twitter.com/TheLayer2DAO/status/1583994186982109185)

    [Layer2DAO Attack Post Mortem](https://medium.com/@layer2dao/after-action-review-of-the-exploit-of-a-layer2dao-multisig-on-october-22-2022-58be7c775aa4) 

    > But since the implementation contract only existed on Arbitrum, the fallback method was hit and this meant: the Gnosis Safe contract was successfully created, but not initialized. This slight distinction was what ultimately allowed the attacker to call the function setup owner on the created but not initialized contract and carry out the attack.

2. [October 27th, Team Finance Hacked](https://rekt.news/teamfinance-rekt/) - [DeFi Platform Exploited for 14.5M Despite Security Audits](https://blockworks.co/defi-platform-exploited-for-14-5m-despite-security-audits/) 
     - [According to SlowMist, a blockchain ecosystem security company, 7M has been returned](https://twitter.com/SlowMist_Team/status/1586913337593384961?s=20&t=PehhQJFmTbZvdV_n39G-JQ)
     - [Exchange with hacker via etherscan chat](https://etherscan.io/idm-chat?addresses=0xBa399a2580785A2dEd740F5e30EC89Fb3E617e6E&title=0xBa399a2580785A2dEd740F5e30EC89Fb3E617e6E)

     - [FortaNetwork spotted the attack 1 hr ahead of time](https://twitter.com/FortaNetwork/status/1586044760476696577?s=20&t=7nivrFSvRR64ewkvpR15xQ) 

3. [October 27th, 2022 - FriesDAO Exploited](https://twitter.com/friesdao/status/1585712229067915264)
    > FriesDAO is a decentralized social experiment where a crypto community builds and governs a fast food franchise empire via wisdom of the crowd

    > On October 27th, 5:58PM UTC, friesDAO contracts were exploited by an attacker taking control of our own deployer address through a profanity attack vector. The hacker was able to drain the treasury of its USDC through the refund contract, drain the FRIES tokens in the staking contract, subsequently selling it all into the Uniswap pool. All transactions in the main attack with the refund contract were confirmed in the same block, then three hours later, the attacker came back for the staking pool

    - part 1 - 2,138,705.403949 USDC
    - part 2 - Took all the FRIES out of the pool, then sold for 120.128930112550592565 ETH ($189,954.761991 at the time):

    - [Nice Post Mortem from the FriesDAO team](https://docs.google.com/document/d/1xKZmj1aeM9iFrdQ7sieUNvh0_UI60worl1lfs5ImXk0/edit) 

4. [Looks like @QuickswapDEX @market_xyz @QiDaoProtocol was exploited on polygon again](https://twitter.com/peckshield/status/1584494792960278528?s=46&t=RQ_Dk8aWQ273JkaiEN2ERA)

6. [Looks @n00dleSwap has an ERC777-based reentrancy issue and is being attacked, causing a loss of $29K.](https://twitter.com/BlockSecTeam/status/1584959295829180416) - [@BlockSecTeam](https://twitter.com/BlockSecTeam/status/1584959295829180416?s=20&t=jnrt9KjksYsbiI5Q-JSEgg)

7. [Dappnode Liquidity Mining Contract Compromised via Profanity vuln](https://twitter.com/DAppNode/status/1586459070608777216) - [Confirmed Profanity Vuln](https://twitter.com/DAppNode/status/1586769316560736256?s=20&t=Gdj2MQTTqZqkJ_hsujcR5A)
    - > The hacker/s ran away with 57.72 ETH and 552.61 GNO (aprox. 165,000 USD)

8. [October 29th, a attacker exploited Giveth GIVfarm using Profanity vuln](https://twitter.com/Givethio/status/1586511169975623682?s=20&t=0HttJMkjSv2apA05O570tA) - [2](https://twitter.com/Givethio/status/1586803946085122051?cxt=HHwWhoC86YPyuoUsAAAA)
    > This was not a smart contract exploit. Rather, the keys we used to control the rate of rewards to our GIVfarms were compromised.

    > The attacker used the compromised keys to change the reward rate for our Mainnet farms to a very large number & then quickly claimed the rewards.
        -[@giveethio](https://twitter.com/Givethio/status/1586511172966273024?s=20&t=0HttJMkjSv2apA05O570tA)

### General Crypto Cyber Crime

1. [Turkey busts an illegal betting organization and seizes $40 million in cryptocurrency](https://cryptoronk.com/turkey-busts-an-illegal-betting-organization-and-seizes-40-million-in-cryptocurrency-report/)
  > According to reports, Turkish officials discovered an illegal betting business that used bitcoins to launder criminal profits.

  > Following the investigation, police enforcement detained 46 people engaged in the fraud and seized $40 million in digital assets.

  > According to the allegations, the company conducted an illicit sports betting scam and transferred the proceeds to multiple bitcoin accounts.

2. [Oct 25th, ZachXBT calls out Monkey Drainer for scamming/phishing ~700ETH in 24hrs](https://twitter.com/zachxbt/status/1584955933452484613?cxt=HHwWioCq4cTB8v4rAAAA)

3. [There is ongoing SIM SWAP attack on @fenbushi! Beware scammers are using their @telegram, @WhatsApp and other messengers accounts and phone numbers!](https://twitter.com/k06a/status/1585373770318897152)


4. [October 20rd - FTX API keys connected to 3Commas confirmed to have been exploited](https://coinmarketcap.com/headlines/news/ftx-api-keys-3commas-exploited/)

5. [OpenSSL to Patch First Critical Vulnerability Since 2016](https://www.securityweek.com/openssl-patch-first-critical-vulnerability-2016)

### Hack Updates
1. [Binance CEO Changpeng Zhao says they are making progress on identifying those responsible for the BNB Bridge hack. In an interview with CNBC he said they received info from LE on a possible suspect](https://dailyhodl.com/2022/10/26/changpeng-zhao-says-binance-zeroing-in-on-identity-of-hacker-behind-570000000-exploit/)

2. [Moola Market Restore Solvency, Resume Deposit And Repayment Functions](https://news.coincu.com/138596-moola-market-restore-solvency/)

2. October Major Incidents from Certik ![certik-oct-hacks-img](../../img/idegen/certik-oct-2022.jpeg) - [Certik Twitter](https://twitter.com/CertiKAlert/status/1587120062766682112?s=20&t=mwhTPquWwdrRNBu6Ct9OHw) 


## I, Degen Links of Interest 

1. [Blockchain Dark Forest Selfguard Handbook](https://darkhandbook.io/) - [Dark Handbook Github](https://github.com/slowmist/Blockchain-dark-forest-selfguard-handbook)

2. ![paladin_marco_tweet](../../img/idegen/paladin_marco_tweet.png)
[-@paladin_marco](https://twitter.com/paladin_marco/status/1584666134363930626?s=21&t=H7FMZxCDikNkN855PsZfQQ)

3. [Inside the elaborate set-up of a scam HQ, staffed by people forced to scam](https://www.channelnewsasia.com/cna-insider/inside-elaborate-set-scam-hq-staffed-people-forced-scam-3018966) 

4. [The SIM Swapping Bible: What To Do When SIM-Swapping Happens To You](https://medium.com/mycrypto/what-to-do-when-sim-swapping-happens-to-you-1367f296ef4d)

5. [A Historical Collection of Reentrancy Attacks](https://github.com/pcaversaccio/reentrancy-attacks)

6. 🛠️ [October 28th, Blocksec launches a transaction pre-execution service - Mopsus. Mopsus aims to help users understand transactions before signing.](https://twitter.com/BlockSecTeam/status/1586024551036596226)


7. 🛠️ [Supermacy Inc launches a visual blockchain transaction explorer "Cruise"](https://twitter.com/Supremacy_CA/status/1586649898031513602)

8. [Coinbase: BSC Token Hub Compromise investigation and analysis](https://www.coinbase.com/blog/bsc-token-hub-compromise-investigation-and-analysis)

9. [We explore empirical evidence on the risk associated with DeFi protocols, that is, the risk of cyberattacks in the form of hacking incidents, rug pulls or economic attacks as a function of the TVLs of the protocols.](https://bittrap.com/resources/defis-growing-pains:-as-tvl-raises-so-does-the-probability-of-being-hacked) - [@andresrieznik](https://twitter.com/andresrieznik) @ Bittrap.com 

  >We estimate that when a DeFi increases its TVL by a factor of 10, its probability of being hacked increases by 10% or 13%, depending on the data considered for the analysis.

10. [A validator received the highest MEV block reward to date from an exploit: 337 Eth](https://www.reddit.com/r/ethstaker/comments/yavd5t/a_validator_received_the_highest_mev_block_reward/)


### Outro
___
**We do our best to report accurately on the topics we discuss, but we're not always going to get everything right. Please contact us [@idegenfm](https://twitter.com/idegenfm), [@WolfDeFi](https://twitter.com/WolfDefi), [@Hunthk11](https://twitter.com/Hunthk11) with corrections or comments!**

