---
title: "E21 - Gala Games Gets Owned By A Whitehat & 50K BTC Silkroad Hacker Pleads Guilty - 11/8/2022"
date: 2022-11-08T12:00:34-07:00
draft: false
images: ["/img/i-degen-banner.png"]
tags:
  - I,DEGEN
  - Crypto
  - podcast
  - Ethereum
  - hacking
  - defi
---

![i-degen-banner-image](/img/i-degen-banner.png)

---
╠ Listen [idegen.fm](https://idegen.fm) ✪ Contact [@idegenfm](https://twitter.com/idegenfm) ✪ Show notes [wolfdefi.com](https://wolfdefi.com/i-degen) ╣

---

#### Intro
Welcome to I, Degen - A podcast about crypto technology, security, and culture. We balance hype and hate, cutting through the misinformation and ego in search of the signal in the noise.

**Listen to I, Degen Episode 21 [directly on idegen.fm](https://share.transistor.fm/s/f45512aa), on [Spotify](), [Apple Podcasts](), or [various other places](https://idegen.fm/subscribe).**

#### E21 Summary 
This week we get into the strange and confusing story of the Gala Games hack, the 50K BTC Silkroad hacker, and a bunch of other crypto security news. 

#### I, Degen - Weekly Crypto Security Headlines of Interest

- Nov 1st, 2022 - [Rogue Actor Disrupts Lightening Network With a Single Transaction](https://www.coindesk.com/tech/2022/11/02/rogue-actor-disrupts-lightning-network-with-a-single-transaction/) - From Coindesk
    - > he allegedly created a non-standard Bitcoin transaction that prevented users from opening new Lightning channels
    - [Issue on Github](https://github.com/btcsuite/btcd/issues/1906)
    - ["Rouge Actor" Tweets](https://twitter.com/brqgoo/status/1588318170821296128)
    ![](../../img/idegen/Burak-the-breaker.png)

- Nov 1st, 2022 - [Gone Phishing for 5M - a ZachXBT Investigation of 0x0Poor aka Elliot](https://zachxbt.mirror.xyz/30AYxD0ecbQlEU2JoabySmIvSAaHWbPe5U4o4VNigL0)

- Nov 1st, 2022 - [Phylum Discovers Dozens More PyPI Packages Attempting to Deliver W4SP Stealer in Ongoing Supply-Chain Attack](https://blog.phylum.io/phylum-discovers-dozens-more-pypi-packages-attempting-to-deliver-w4sp-stealer-in-ongoing-supply-chain-attack)
    - malicous packages are clones with addational import statements 
    - [But what does W4ASP Do?](https://news.ycombinator.com/item?id=33439970) 
    > It downloads a script that, at least right now, will turn around and grab cookies and passwords from browsers and send the data off to an discord webhook.

    - [W4SP Source Code](https://github.com/loTus04/W4SP-Stealer)

- Nov 1st, 2022 - [Crypto exchange Deribit loses $28 million in hack](https://www.theblock.co/post/182048/crypto-exchange-deribit-loses-28-million-in-hack-halts-withdrawals) - From The Block
    - [Deribit Tweet On the Hack](https://twitter.com/DeribitExchange/status/1587701883778523136)
    - Nov 7th - [Deribit Hacker Moving Stolen ETH Through Tornado Cash](https://cointelegraph.com/news/deribit-hackers-move-stolen-ether-to-tornado-cash-crypto-mixer)

- Nov 2nd, 2022 - [OpenSea Now Auto-Detects and Blocks Stolen NFTs, Disables Scam Links](https://decrypt.co/113332/opensea-now-auto-detects-blocks-stolen-nfts-disables-scam-links)
    - OpenSea, the top NFT marketplace by trading volume, has launched new theft detection and prevention features.
    - One feature detects and disables scam links shared on the platform, while the other identifies stolen NFTs and blocks their resale.
    > The tool automatically scans any links that users entered on the marketplace and disables any that point to known scams, or that redirect clickers to websites with malicious code that could swipe NFTs from someone’s wallet.
    > On one hand, the tool relies on an expanding blocklist tracking identified exploits. But it also goes one step further by simulating transactions through any wallet connectivity prompts on the linked website

- Nov 2nd, 2022 - From The Block [1K Solana Validators Go Offline as Hetzner blocks server access](https://www.theblock.co/post/182283/1000-solana-validators-go-offline-as-hetzner-blocks-server-access)
    ![](../../img/idegen/solana-servers-denied.png)
    - sounds like there are plenty of ETH nodes hosted here too, likely soon they will be shut down?

- Nov 2nd, 2022 - From Rubic Team on Twitter [Cross chain DEX Rubic's has an admin wallet address compromised.](https://twitter.com/CryptoRubic/status/1587704548688367619)
    - [One post on the hack, no mention of how the wallet was compromised though. Profanity...?](https://cryptorubic.medium.com/rubic-weekly-report-11-04-2022-ce6196be68b8)
    - [@CryptoRubic's post-mortem of sorts](https://twitter.com/CryptoRubic/status/1589649809274990592)

- Nov 3rd, 2022 - [SKYWARD FINANCE Treasure Drained for 3.2M](https://rekt.news/skyward-rekt/) - From Rekt 
    > the function lacks proper verification of the token_account_ids parameter, allowing the attacker to loop the redemption of wNEAR by repeatedly passing their withdrawal within the transaction.
    > The fact that it took over a year for anyone to find this relatively simple exploit is remarkable.

- Nov 5th, 2022 - [Pando Rings is hacked by someone.](https://twitter.com/pando_im/status/1589113232899575809) - @pando_im
    - [Pando Rings, Leaf, Lake and 4swap protocol suspended due to price oracle attack, will be back as soon as possible.](https://twitter.com/pando_im/status/1589045252413100032)
    - I've been trying to find DeFi hacks that aren't being reported on, this one comes via ETHSecurity TG via ETHMsg TG

- Nov 3rd, 2022 [Arbitrage traders make over $6.5M from Gala Games scare](https://cryptoslate.com/arbitrage-traders-make-over-6-5m-from-gala-games-scare/)
    - PeckShield tweets about a possible hack
    - The pNetwork team quickly said that it noticed that pGALA tokens were no longer safe, so it coordinated the attack to prevent malicious players from taking advantage of the situation. 
    - > The Gala Games token fell by roughly 30% due to a white hat attack that printed $1 billlion new tokens.
    - But, there is more [Gala Whitehat Hack Nets 4M](https://cryptoslate.com/per-huobi-alleges-pnetwork-made-over-4m-from-its-gala-white-hat-attack/) - From CryptoSlate
    - > Huobi has alleged that the GALA incident was not a white hat operation as pNetwork claimed, but an attack that gave the protocol a $4.5 million profit.

        > According to the exchange, there were other “premeditated operations” during the incident, which resulted in over $10 million in profit for the parties involved. Huobi continued that the pNetwork team did not inform it that it would attack the pGALA token despite communicating with it 50 minutes before the incident.
    - [Nice looking investigation from @lookonchain](https://twitter.com/lookonchain/status/1588352240888053760?s=20&t=RHQ50bk0XdzJ7Ei4hyWT4Q)

- Nov 7th, 2022 - [U.S. Attorney Announces Historic $3.36 Billion Cryptocurrency Seizure And Conviction In Connection With Silk Road Dark Web Fraud](https://www.justice.gov/usao-sdny/pr/us-attorney-announces-historic-336-billion-cryptocurrency-seizure-and-conviction)
    - > Damian Williams, the United States Attorney for the Southern District of New York, and Tyler Hatcher, the Special Agent in Charge of the Internal Revenue Service, Criminal Investigation, Los Angeles Field Office (“IRS-CI”), announced today that JAMES ZHONG pled guilty to committing wire fraud in September 2012 when he unlawfully obtained over 50,000 Bitcoin from the Silk Road dark web internet marketplace.  ZHONG pled guilty on Friday, November 4, 2022, before United States District Judge Paul G. Gardephe. 

    - [140 rapid withdrawals in rapid succession, by nine fake accounts](https://mobile.twitter.com/moo9000/status/1589631175693594624)

#### I, Degen - Other Links of Interest
- Oct 21st, 2022 - 🛠️ [Web3AV? Strange.](https://medium.com/@w3a/introducing-web3-antivirus-e281763dee13)

- Oct 23rd, 2022 - [Web3 Developer Report (Q3 2022)
](https://www.alchemy.com/blog/web3-developer-report-q3-2022)

- Nov 1st, 2022 - [‘I was a slave’: Up to 100,000 held captive by Chinese cybercriminals in Cambodia](https://www.latimes.com/world-nation/story/2022-11-01/i-was-a-slave-up-to-100-000-held-captive-by-chinese-cyber-criminals-in-cambodia) - From The LATimes 
    > Dumped in a high-rise building above a casino, he was turned over to mobsters who seized his passport and put him to work bilking gamblers on a sham sports betting app.

- Nov 2nd, 2022 - [New Stealer In The Wild](https://twitter.com/sniko_/status/1587875104729534465?t=X40FawAzTPrViRyxtx-ViA&s=19)

- Nov 3rd, 2022 - [Yearn security team has discovered a vulnerability in a third-party BribeV2 contract.](https://twitter.com/iearnfinance/status/1587725515485577217?s=46&t=aBf20--kIQ_Hrkqph1bv9g)

- Nov 3rd, 2022 - [A theft ring that allegedly made millions from catalytic converters has been busted](https://www.npr.org/2022/11/03/1133788485/catalytic-converters-theft-ring-federal-fbi) - From NPR 

- Nov 3rd, 2022 - [Zellic is proud to be the first audit firm backing their audit with a real stake.](https://twitter.com/zellic_io/status/1588344731611779072)

- Nov 4th, 2022 - [Run Bitcoin Run Puzzle Solved](https://medium.com/@Fiach_dubh/14-million-satoshi-puzzle-run-bitcoin-run-solved-4462b81e291)

- Nov 6th, 2022 - [IRS is building hundreds of Crypto tax evasion cases](https://cryptopotato.com/irs-is-building-hundreds-of-crypto-tax-evasion-cases/)

- [Telegram Feed of all Ethereum messages that contain message data](https://t.me/ethmsg) - This was linked in ETHSecurity TG and looks like a cool way to potentially spot low-profile events of interest. 

- [Immunefi WhiteHat Leader Board](https://immunefi.com/leaderboard/) 
    - Top hacker has earned 13M as of this writing. Wow. 

