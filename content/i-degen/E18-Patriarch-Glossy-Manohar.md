---
title: "E18 - Patriarch Glossy Manohar, you are being liquidated!"
date: 2022-10-11T14:12:10-06:00
draft: false
images: ["/img/i-degen-banner.png", "/img/idegen/rabby-swap-exploit-1.png"]
tags:
  - I,DEGEN
  - Crypto
  - podcast
  - Ethereum
  - hacking
  - defi
---

![i-degen-banner-image](/img/i-degen-banner.png)

---
╠ Listen at [idegen.fm](https://idegen.fm) ✪ Contact [@idegenfm](https://twitter.com/idegenfm) ✪ Show notes [wolfdefi.com](https://wolfdefi.com/i-degen) ╣

---

### **You can listen to this episode [HERE](https://idegen.fm/episodes/e18-patriarch-glossy-manohar-you-are-being-liquidated-10-14-2022)**

#### Intro
Welcome to I, Degen - A podcast about crypto technology, security, and culture. With a healthy balance of enthusiasm and skepticism, we dig into a weekly look at crypto, cutting through the misinformation and hype in search of signal in the noise.

#### I, Degen - Episode 18 Summary - October 2nd-14th, 2022
It's been another big week of hacks in crypto land, and depending on how you keep score, it might be the biggest ever. We saw multiple chains and bridges get hacked, price oracle manipulation, wallet exploits, etc. We look into the Binance bridge attack to Mango Markets, Sovryn, QANplatform, Sybil attacks on XEN, Temple DAO, and more.

![mango attack account screen shot](../../img/idegen/mango-markets-hacker.png)

## I,Degen - Weekly Crypto, Security, & Other News Headlines of Interest

1. [Bongbong Marcos signs SIM Card Registration Act](https://www.reddit.com/r/privacy/comments/y0aqs8/bongbong_marcos_signs_sim_card_registration_act/)
2. [Cyber sleuth alleges $160M Wintermute hack was an inside job ](https://cointelegraph.com/news/cyber-sleuth-alleges-160m-wintermute-hack-was-an-inside-job)
3. [Why Celsius Doxxed Hundreds of Thousands of Users](https://www.coindesk.com/layer2/2022/10/10/why-celsius-doxxed-hundreds-of-thousands-of-users/)
4. [Hustler Casino Live discovers employee stole chips from Robbi after sketchy J4 hand](https://twitter.com/HCLPokerShow/status/1578169889788862464?s=20&t=8LnhCq6Dg-8P3XcZcJU9ig)
5. [Hacking Google Mini-series (6 EPs)](https://www.reddit.com/r/cybersecurity/comments/xxytkt/google_miniseries_hacking_google/) gives a nice inside look into their security & incident response teams.
6. [Signal is secure, as proven by hackers](https://www.kaspersky.co.uk/blog/signal-hacked-but-still-secure/24864/)
7. [Sleuth Discovers Satoshi’s Long-Lost Bitcoin Version 0.1 Codebase, Raw Code Contains Bitcoin Inventor’s Never-Before-Seen Personal Notations](https://news.bitcoin.com/sleuth-discovers-satoshis-long-lost-bitcoin-version-0-1-codebase-raw-code-contains-bitcoin-inventors-never-before-seen-personal-notations/)
8. [State-run live TV hacked by protesters](https://www.bbc.com/news/world-middle-east-63188795)
9. [@markrussinovich on ETH PoW](https://twitter.com/markrussinovich/status/1579242133797892096?s=20&t=CzBQE93ZoZYmHl5d-qupRA)
![](../../img/idegen/mark-r-tweet.png)
10. [Copy/Paste frontrunner bot is not what it seems (shockinly)](https://www.reddit.com/r/solidity/comments/y073rb/fat_scammer_just_want_to_know_how_the_code_works/)
11. "Probably better if we don’t treat random anon tweets like they’re reported facts."[\--@haydenzadams](https://twitter.com/haydenzadams/status/1579642078619111425?s=46&t=kxq0yL3cprI3XHOp_x2pGg)
![](../../img/idegen/hayden-tweet.png)
12.[Update on crypto.com accidentally sending user $10.47MM instead of $100, she's out on bail awaiting trial](https://www.theguardian.com/technology/2022/oct/11/crypto-com-accidental-transfer-10-5-million-trial-australia-couple-cryptocurrency)
13. [Denver International Airport target in cyber-attack](https://news.yahoo.com/denver-international-airport-target-cyber-042355176.html?guccounter=1)
14. Tether freezes address with 3.4MM [in USDT](https://twitter.com/whale_alert/status/1579591783805489152?s=20&t=KUzACyUc2H-PQ4sxBZUAOQ), bringing [total frozen](https://cointelegraph.com/news/tether-stablecoin-issuer-freezes-8-2m-usdt-on-ethereum-data) addresses to 215
15. David Hoffman, Coincenter, and others suing US Treasury Department over TornadoCash sanctions [@TrustlessSTate](https://twitter.com/TrustlessState/status/1580253035804372993)
![](../../img/idegen/david-hoffman-tweet.png)
16. [New York changes gun buyback after seller gets $21,000 for 3D-printed parts](https://www.theguardian.com/us-news/2022/oct/11/new-york-gun-buyback-rules-3d-printed-parts)
17. [A Quarter of SEC Employees Stock Invested in Firms Lobbying SEC](https://www.trustnodes.com/2022/10/12/a-quarter-of-sec-employees-stock-invested-in-firms-lobbying-their-agency)
18. Rabby Wallet Swap Exploit One Month After Launch [@rabby_io](https://twitter.com/rabby_io/status/1579819525494960128)

![Rabby Wallet exloit tweet image](../../img/idegen/rabby-swap-exploit-2.png)

## I,Degen - Weekly Crypto Hacks Deep Dive

### 1. October 6th - Binance Smart Chain Token Hub hack 
> BSC Token Hub, the BNB bridge between the old Binance Beacon Chain and BSC, now BNB Chain… was exploited into minting two lots of 1M BNB directly to the hacker’s address. - [Rekt.news](https://rekt.news/bnb-bridge-rekt/)

> In summary, there was a bug in the way that the Binance Bridge verified proofs which could have allowed attackers to forge arbitrary messages. Fortunately, the attacker here only forged two messages, but the damage could have been far worse - [@samczsun Twitter](https://twitter.com/samczsun/status/1578167198203289600) 

> When the BNB team halted the chain, approximately 90 mins after the second transaction, the hacker lost access to the ~$430M still on their BSC address. The hacker’s addresses were initially funded from ChangeNOW exchange.

  - [BNB Bridge hack ELI5 explained and visualised](https://drdr-zz.medium.com/bnb-bridge-hack-eli5-explained-and-visualised-1fb2837c7a7e) h/t-->[@drdr_zz](https://twitter.com/drdr_zz)
  - [@emilianobonassi](https://twitter.com/emilianobonassi/status/1578742880662716416)  
  - [Techcrunch - Binance hit by $100 million blockchain bridge hack](https://techcrunch.com/2022/10/07/blockchain-bridge-hack/)
### 2. October 4th, Sovryn Hack 
> ~$1.1M was stolen from Sovryn, a “DeFi” protocol on the “Bitcoin smart contract network”, RSK

> An attacker exploited the legacy Lend/Borrow protocol to inappropriately withdraw funds 

The exploit manipulated the iToken price through a clever flashswap, loan, and lp combination. 

The attacker manipulated iRBTC price so that they could take out much more RBTC than they initially deposited.

> The attack was detected by Sovryn devs and the system placed into maintenance mode 

> Due to the multi-layered security approach taken, devs were able to identify and recover funds as the attacker was attempting to withdraw the funds. 

> Sovryn spokesperson Edan Yago said this is the first successful exploit against the protocol after two years of operation. He maintained that Sovryn is “one of the most heavily audited Defi systems,” with valuable and active bug bounties.

- [Sovryn Interim Exploit Update](https://www.sovryn.app/blog/interim-exploit-update) - From Sovryn
- [Sovryn Hack](https://rekt.news/sovryn-rekt/) - From Rekt.news
- [Bitcoin Defi Protocol Sovryn Gets Hacked for Over $1 Million](https://cryptopotato.com/bitcoin-defi-protocol-sovryn-gets-hacked-for-over-1-million/) - via CryptoPotato
### 3. October 3rd, Transit Swap update. 
[Hacker returned~70%, or 18.9M that they stole](https://medium.com/@TransitSwap/updates-about-transitfinance-4731c38d6910) 
from the cross chain DEX after reports that security firms had doxxed hacker IPs. Later in the week, Transit began allowing users to claim stolen funds. 

More from [coindesk](https://www.coindesk.com/business/2022/10/03/transit-swap-exploiter-returns-large-chunk-of-289m-hack/) and [link to hack breakdown from SlowMist](https://slowmist.medium.com/cross-chain-dex-aggregator-transit-swap-hacked-analysis-74ba39c22020)

### 4. Around 22:00 UTC on October 11th, Mango Markets Get Owned
> Solana’s flagship margin trading protocol lost 9 figures to a well-funded market manipulator. The attacker managed to spike the price of Mango Markets’ native token MNGO and drain their lending pools, leaving the protocol with $115M of bad debt. - [via rekt.news](https://rekt.news/mango-markets-rekt/)
  - The team was warned about the potential for such an attack in March of 2022, more than six months ago. 
  - The attacker created a proposal to solve the problem they started using their newly acquired fat stack of MNGO tokens [dao.mango.markets](https://dao.mango.markets/dao/MNGO/proposal/3WZ5DpZXDvNAK4JwPS1HDPzSinEJUGpBC4XXx9vPtnVS). As you might expect, the thread has become heated. 
  - and now we have a [new proposal](https://dao.mango.markets/dao/MNGO/proposal/GYhczJdNZAhG24dkkymWE9SUZv8xC4g8s9U8VF5Yprne) that has reached quorum  

### 5. TempleDAO, yield-farming decentralized finance (DeFi) protocol, lost over $2.34 million (1830 ETH) to a hack on Oct. 11. 
![](../../img/idegen/0xfoobar-tweet.png)

[\-- 0xfoobar Tweet](https://twitter.com/0xfoobar/status/1579850727283101697?s=46&t=Bqa6kG8bqjH7QneDD7gC1w)

[TempleDAO exploit results in $2M loss](https://cointelegraph.com/news/templedao-exploit-results-in-2m-loss) --Cointelegraph 

The hack raises questions like, "how much more money is sitting out there easy for the taking?"

### 6. A security firm claims that Paraswap deployer address was generated by Profanity 
- Paraswap denies claim
- BlockSecTeam proceeds to generate a TX from the account as proof that the private key has been compromised. 
- Either way, the deployer address has no power on the contract. 
- Curve gets in the mix, as they were also accused, and pushes back: 
> When will some auditors stop reporting nonissues for hyping themselves?
- [Full thread @BlockSecTeam](https://twitter.com/BlockSecTeam/status/1579769525247279104?ref_src=twsrc%5Etfw)

### 7. RIP XEN - [Someone abused FTX’s withdrawal fee subsidy to mint $70,000 of XEN](https://www.theblock.co/post/176923/someone-abused-ftxs-withdrawal-fee-subsidy-to-mint-70000-of-xen?utm_source=telegram1&utm_medium=social) 

h/t -> [@vishal4c](https://twitter.com/vishal4c) 

[The newly launched XEN Crypto is down 39% in the last 24 hours after a user found a way to mint over 100 million tokens on FTX without paying any gas fees.](https://cryptoslate.com/xen-crypto-down-39-as-it-suffers-multiple-attacks/)

> The attacker deployed a contract to launch the attack on Oct. 10. They then used the FTX exchange hot wallet address to continuously transfers small amounts of ETH to the attack contract. Each transaction creates 1 to 3 subcontracts, and these then perform a mint to claim the XEN tokens. All of these are paid for by the FTX hot wallet address.

> The project is also experiencing a Sybil attack, with 80% of participating addresses being Sybil addresses. -- [Sybil Attack WARNING--XEN Crypto](https://mirror.xyz/x-explore.eth/qcksnJCLS9tbUm7moCoGLIB5aDXpUyayfWm9I-0VP0Y)

- XEN's massive gas usage has been a large part of higher gas fees and recent gas burn? 

### 8. On the 11th of October, 2022, the quantum-resistant QANplatform suffered a severe blow when the QANX Bridge deployer wallet was compromised. 

> ...it was created using an open source vanity address calculation algorithm called cenut/vanity-eth-gpu which is a derivative of a compromised upstream project called johguse/profanity. - [qanplatform medium](https://medium.com/qanplatform/qanx-bridge-wallet-disclosure-analysis-continuously-updated-724121bbbf9a)

![QAN faq from hack article](../../img/idegen/QAN-profanity-hack-faq.png)

-- [@QANplatform](https://twitter.com/QANplatform/status/1579759166478254080) 

-- [QANX token collapse after wallet hack](https://cryptopotato.com/qanx-token-collapses-90-after-1-million-bridge-hack/) 

### I, Degen - Freestyle Convo
Hunt's update from Devcon VI

### I, Degen - Personal Hack Attempt of the Week
Too many DeFi hacks this week. We'll pick this up next week 💀 

### Other Links

On the show I said I would like to a good site for removing approvals. Use [Revoke Cash](https://revoke.cash/) to remove any old approvals from your accounts. 

___
**We do our best to report accurately on the topics we discuss, but we're not always going to get everything right. Please reach out to us @idegenfm, @WolfDeFi, @Hunthk11 with corrections or comments!**
