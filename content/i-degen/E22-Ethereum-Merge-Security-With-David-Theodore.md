---
title: "E22 - Securing the Merge W/David Theodore From The Ethereum Foundation - 11/15/2022"
date: 2022-11-11T08:07:21-07:00
draft: false
images: ["/img/i-degen-banner.png"]
tags:
  - I,DEGEN
  - Crypto
  - podcast
  - Ethereum
  - hacking
  - defi
  - Ethereum Foundation
---

╠ Listen [idegen.fm](https://idegen.fm) ✪ Contact [@idegenfm](https://twitter.com/idegenfm) ✪ Show notes [wolfdefi.com](https://wolfdefi.com/i-degen) ╣

---

#### Intro
Welcome to I, Degen - A podcast about crypto technology, security, and culture. We balance hype and hate, cutting through the misinformation and ego in search of the signal in the noise.

**Listen to I, Degen Episode 22 [directly on idegen.fm](https://idegen.fm/episodes/e22-securing-the-merge-w-david-theodore-from-the-ethereum-foundation-11-15-2022), on [Spotify](https://open.spotify.com/episode/2PcpukpRpUEY5TGSOfBdDX?si=60796e5ae3e644cc), [Apple Podcasts](https://podcasts.apple.com/us/podcast/i-degen/id1643133657), or [various other places](https://idegen.fm/subscribe).**

#### E22 Summary 
This week we talk with David Theodore, a Security Researcher at the Ethereum Foundation about how they pulled off the merge securely, Proof of Stake, and various other topics.  

### Outro
___
**We do our best to report accurately on the topics we discuss, but we're not always going to get everything right. Please contact us [@idegenfm](https://twitter.com/idegenfm), [@WolfDeFi](https://twitter.com/WolfDefi), [@Hunthk11](https://twitter.com/Hunthk11) with corrections or comments!**